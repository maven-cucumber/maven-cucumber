@testYouAppTesting1
Feature: Verify Login Screen with Multiple sets of data

  @dataTableLogin
  Scenario: As a user I should test scenario outline
    Given I want to navigate to the website url
      | urlname                          |
      | http://www.testyou.in/Login.aspx |
    And I enter correct credentails in text fields with below data
      | username        | passwordname |
      | seleniumtesting | 123456sk     |
    When I click on login button
    Then I validate the successfull login with logout link
