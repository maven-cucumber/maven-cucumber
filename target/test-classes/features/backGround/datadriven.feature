@VerifyDataDriven
Feature: Title of your feature

  Background: 
    Given I want to navigate to urlname
    And I enter correct credentails
      | userid                  | pwdid      |
      | vapprod1@mailinator.com | password@2 |
    When I click on login button
    Then I successfully login into application

  @VerifyLogoutLink
  Scenario: As a user I should validate logout link
    Then user gets login successfull without errors and verify logout link

  @verifyHomeLink
  Scenario: As a user I should validate home link
    Then user gets login successfull without errors and verify home link
