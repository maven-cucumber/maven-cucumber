@testYouAppTesting2
Feature: Verify Login Screen with Multiple sets of data

    @outlineLogin
  Scenario Outline: As a user I should login into Hotel Application site
    Given user navigate to the hotel url
    When user enters "<UserName>" in username field
    And user enters "<PasswordName>" in password field
    And user clicks Sign in button
    Then user gets login successfull error message
    
    Examples: 
      | UserName        | PasswordName |
      | seleniumtesting | password@1   |
      | seleniumtesting | testing123   |
