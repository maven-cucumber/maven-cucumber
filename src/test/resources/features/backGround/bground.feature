@testYouAppTesting3
Feature: Verify Login Screen with Multiple sets of data

  Background: 
    Given user enters in username and password
    And user clicks on login button

  @Verify1
  Scenario: As a user I should login success
    Then user gets login successfull without errors and verify logout link

  @verify2
  Scenario: As a user I should not login
    Then user gets login successfull without errors and verify home link
