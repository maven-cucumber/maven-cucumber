package com.selenium.autotest.stepdefin.testNatural;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class testingNaturals {
	
	public static WebDriver driver;
	
	@Given("^I launch web application$")
	public void i_launch_web_application() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		
	}

	@When("^I enter correct login details$")
	public void i_enter_correct_login_details() {
	
	}

	@Then("^I validate logout link$")
	public void i_validate_logout_link() {
	
	}


}
