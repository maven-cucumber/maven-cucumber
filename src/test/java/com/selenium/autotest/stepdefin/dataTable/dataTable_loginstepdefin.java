package com.selenium.autotest.stepdefin.dataTable;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class dataTable_loginstepdefin {

	public static WebDriver driver;

	@Before
	public void startBrowser() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();

	}
	
	@Given("^I want to navigate to the website url$")
	public void navigate_url(DataTable dt) {

		List<List<String>> list = dt.asLists(String.class);
		for (int i = 1; i < list.size(); i++) { // i starts from 1 because i=0 represents the header
			driver.get(list.get(i).get(0));
		}

	}

	@Given("^I enter correct credentails in text fields with below data$")
	public void enter_credentails(DataTable dt) {
		List<Map<String, String>> list = dt.asMaps(String.class, String.class);
		
		//System.out.println(list.get(0).get("username"));
		//System.out.println(list.get(0).get("passwordname"));
		
		for (int i = 0; i < list.size(); i++) {
			driver.findElement(By.xpath("//input[@type='text']")).sendKeys(list.get(i).get("username"));
			driver.findElement(By.xpath("//input[@type='password']")).sendKeys(list.get(i).get("passwordname"));
		}

	}

	@When("^I click on login button$")
	public void click_login_button() {

		driver.findElement(By.xpath("//input[@value='Login']")).click();

	}

	@Then("^I validate the successfull login with logout link$")
	public void verify_succesfull_login() {

		WebElement logoutLink = driver.findElement(By.xpath("//a[@id='ctl00_headerTopStudent_lnkbtnSignout']"));

		if (logoutLink.getText().equals("LOG OUT")) {
			System.out.println("Login Successfull " + logoutLink.getText());
		} else {
			System.out.println("Login Failed " + logoutLink.getText());
		}

	}
	
	@After
	public void closeBrowser() throws InterruptedException {
		Thread.sleep(5000);
		driver.quit();
	}

}
