package com.selenium.autotest.stepdefin.bGround;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class bGround_loginstepdefin {

	public static WebDriver driver;

	@Before
	public void before() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.get("http://adactinhotelapp.com/index.php");
	}

	@After
	public void after() {
		WebElement logoutLink = driver.findElement(By.linkText("Logout"));

		if (logoutLink.getText().equals("Logout")) {
			logoutLink.click();
		} else {
			System.out.println("Login Failed " + logoutLink.getText());
		}

	}

	@Given("^user enters in username and password$")
	public void user_enters_in_username_and_password() {

		driver.findElement(By.xpath("//input[@id='username']")).sendKeys("seleniumtesting");
		driver.findElement(By.xpath("//input[@id='password']")).sendKeys("password1");
	}

	@Given("^user clicks on login button$")
	public void user_clicks_on_login_button() {
		driver.findElement(By.xpath("//input[@id='login']")).click();
	}

	@Then("^user gets login successfull without errors and verify logout link$")
	public void user_gets_login_successfull_without_errors_and_verify_logout_link() {

		WebElement logoutLink = driver.findElement(By.linkText("Logout"));

		if (logoutLink.getText().equals("Logout")) {
			logoutLink.click();
		} else {
			System.out.println("Login Failed " + logoutLink.getText());
		}
	}

	@Then("^user gets login successfull without errors and verify home link$")
	public void user_gets_login_successfull_without_errors_and_verify_home_link() {

		WebElement logoutLink = driver.findElement(By.xpath("//a[contains(text(),'Home')]"));

		if (logoutLink.getText().equals("Home")) {
			System.out.println("Login Successfull " + logoutLink.getText());
		} else {
			System.out.println("Login Failed " + logoutLink.getText());
		}

	}
}
