package com.selenium.autotest.stepdefin.datadriven;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class datadriven_bGround {
	public static WebDriver driver;

	@Given("^I want to navigate to urlname$")
	public void i_want_to_navigate_to_urlname() {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.get("http://testyou.in/index.aspx");
		driver.findElement(By.linkText("Login")).click();
	}

	@Given("^I enter correct credentails$")
	public void i_enter_correct_credentails(io.cucumber.datatable.DataTable dataTable) {
		driver.findElement(By.xpath("//input[@type='text']")).sendKeys("seleniumtesting");
		driver.findElement(By.xpath("//input[@type='password']")).sendKeys("123456sk");
	}

	@When("^I click on login button$")
	public void i_click_on_login_button() {
		driver.findElement(By.xpath("//input[@value='Login']")).click();
	}

	@Then("^I successfully login into application$")
	public void i_successfully_login_into_application() {
		String verifyTitle = driver.getTitle();
		if (verifyTitle.equals("My Agency Home")) {
			System.out.println("Login Successfull " + driver.getTitle());
		} else {
			System.out.println("Login Failed " + driver.getTitle());
		}
	}

	@Then("^user gets login successfull without errors and verify logout link$")
	public void user_gets_login_successfull_without_errors_and_verify_logout_link() {
		WebElement logoutLink = driver.findElement(By.xpath("//a[@id='ctl00_headerTopStudent_lnkbtnSignout']"));
		if (logoutLink.getText().equals("LOG OUT")) {
			System.out.println("Login Successfull " + logoutLink.getText());
		} else {
			System.out.println("Login Failed " + logoutLink.getText());
		}
	}

	@Then("^user gets login successfull without errors and verify home link$")
	public void user_gets_login_successfull_without_errors_and_verify_home_link() {
		WebElement logoutLink = driver.findElement(By.xpath("//a[contains(text(),'Home')]"));
		if (logoutLink.getText().equals("Home")) {
			System.out.println("Login Successfull " + logoutLink.getText());
		} else {
			System.out.println("Login Failed " + logoutLink.getText());
		}
	}
}
