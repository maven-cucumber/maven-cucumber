package com.selenium.autotest.stepdefin.scenarioOutline;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

public class scenarioOutLine_loginstepdefin {

	public static WebDriver driver;
	
	@Before
	public void startBrowser() {
		
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();

	}
	
	@Given("^user navigate to the hotel url$")
	public void navigateURL() {
		
		
		driver.get("http://adactinhotelapp.com/");
	}

	
	@When("^user enters \"([^\"]*)\" in username field$")
	public void user_enters_seleniumtesting_in_username_field(String userName) {
		driver.findElement(By.xpath("//input[@id='username']")).sendKeys(userName);

	}
	
	@When("^user enters \"([^\"]*)\" in password field$")
	public void user_enters_password_in_password_field(String passwordName) {
		driver.findElement(By.xpath("//input[@id='password']")).sendKeys(passwordName);
 
	}
	
	@When("^user clicks Sign in button$")
	public void clickLoginButton() {
		
		driver.findElement(By.id("login")).click();
		
	}


	@Then("^user gets login successfull error message$")
	public void ValidateSucessfullLogin() {
		WebElement getLinkText = driver.findElement(By.linkText("Logout"));

		if (getLinkText.getText().equals("Logout")) {
			System.out.println("Login Successfull");
		} else {
			System.out.println("Login Failed");
		}

		
	}


	@After
	public void closeBrowser() throws InterruptedException {
	
		Thread.sleep(5000);
		driver.close();
	}


}
