package com.selenium.autotest.runner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources/features/dataTable",glue= {"com.selenium.autotest.stepdefin.dataTable"},tags= {"@testYouAppTesting1"})
public class dataTable_loginTestRunner {

}
