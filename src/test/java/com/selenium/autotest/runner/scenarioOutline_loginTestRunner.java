package com.selenium.autotest.runner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources/features/scenarioOutline",glue= {"com.selenium.autotest.stepdefin.scenarioOutline"},tags= {"@outlineLogin"})
public class scenarioOutline_loginTestRunner {

}
