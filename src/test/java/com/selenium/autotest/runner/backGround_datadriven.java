package com.selenium.autotest.runner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources/features/backGround",
tags= {"@VerifyDataDriven"},dryRun=false)
public class backGround_datadriven {

}
