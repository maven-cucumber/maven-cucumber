package com.selenium.autotest.runner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources/features/testNatural/loginApp.feature",
dryRun=false,glue= {"com.selenium.autotest.stepdefin.testNatural"})
public class testNatural_loginTestRunner {

}


